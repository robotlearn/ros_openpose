FROM nvidia/cuda:11.4.2-cudnn8-devel-ubuntu20.04
SHELL ["/bin/bash", "-c"]
WORKDIR /
RUN echo "Setting up timezone..." && \
    echo 'Etc/UTC' > /etc/timezone && \
    ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime
RUN echo "Installing Python and Pytorch..." && \
    apt-get update && \
    apt-get install -q -y --no-install-recommends \
        python3-dev \
        python3-pip \
        tzdata && \
    pip3 install torch torchvision torchaudio && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /workspace
RUN echo "Installing ROS Noetic..." && \
    apt-get update && \
    apt-get install -q -y --no-install-recommends \
        curl && \
    echo "deb http://packages.ros.org/ros/ubuntu focal main" > \
        /etc/apt/sources.list.d/ros-latest.list && \
    curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | \
        apt-key add - && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        ros-noetic-desktop-full && \
    rm -rf /var/lib/apt/lists/* && \
    echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc

RUN echo "Installing OpenPose..." && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        build-essential \
        git \
        libatlas-base-dev \
        libboost-all-dev \
        libgflags-dev \
        libcanberra-gtk-module \
        libgoogle-glog-dev \
        libhdf5-serial-dev \
        libleveldb-dev \
        liblmdb-dev \
        libopencv-dev \
        libprotobuf-dev \
        libsnappy-dev \
        libviennacl-dev \
        ocl-icd-opencl-dev \
        opencl-headers \
        pciutils \
        protobuf-compiler \
        python3-setuptools && \
    pip3 install \
        numpy \
        opencv-python \
        protobuf && \
    rm -rf /var/lib/apt/lists/* && \
    cd /home && \
    git clone --depth 1 --branch 'v1.7.0' \
        https://github.com/CMU-Perceptual-Computing-Lab/openpose && \
    mkdir -p /home/openpose/build && cd /home/openpose/build && \
    cmake -DBUILD_PYTHON=ON .. && \
    sed -ie 's/set(AMPERE "80 86")/#&/g' ../cmake/Cuda.cmake && \
    sed -ie 's/set(AMPERE "80 86")/#&/g' ../3rdparty/caffe/cmake/Cuda.cmake && \
    make -j `nproc` && \
    make install
RUN apt-get update && apt-get install -y wget vim tmux
RUN cd /home/openpose/models && bash getModels.sh
RUN mkdir -p /home/ros/op_ws/src
WORKDIR /home/ros/op_ws/
ADD ros_openpose /home/ros/op_ws/src/ros_openpose
RUN mkdir -p src/spring_msgs
ADD spring_msgs src/spring_msgs
RUN /bin/bash -c "source /opt/ros/noetic/setup.bash && catkin_make" 
RUN source devel/setup.bash && roscd ros_openpose/scripts && chmod +x *
RUN ln -s /usr/bin/python3 /usr/bin/python
RUN echo "source /opt/ros/noetic/setup.bash && source /home/ros/op_ws/devel/setup.bash" >> /root/.bashrc

